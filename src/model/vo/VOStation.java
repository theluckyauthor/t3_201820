package model.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Representation of a station object
 */
public class VOStation{

private int id;
private String name;
private String city;
private double latitude;
private double longitude;
private int dpcapacity;
private Date online_date;


public VOStation(int pid, String pname, String pcity, double platitude, double plongitude, int pdpcapacity, String ponline_date) throws Exception
{
	id = pid;
	name = pname;
	city = pcity;
	latitude = platitude;
	longitude = plongitude;
	dpcapacity = pdpcapacity;
	online_date =  new SimpleDateFormat ("MM/dd/yyyy kk:mm").parse(ponline_date);
}
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getCity() {
		return city;
	}
	public double getLatitude() {
		return latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public int getdpCapacity() {
		return dpcapacity;
	}
	public Date getonline_date() {
		return online_date;
	}
}
