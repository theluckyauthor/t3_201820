package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Iter;
import model.data_structures.LinkedList;
import model.data_structures.ListQueue;
import model.data_structures.ListStack;


public class DivvyTripsManager implements IDivvyTripsManager {

	private ListStack <VOStation> stationStack;
	private ListStack <VOTrip> tripStack;
	private ListQueue <VOStation> stationQueue;
	private ListQueue <VOTrip> tripQueue;

	public void loadStations (String stationsFile) {
		stationStack = new ListStack<VOStation>();
		stationQueue = new ListQueue <VOStation>();
		try
		{
			FileReader fr =  new FileReader(stationsFile);
			BufferedReader br = new BufferedReader(fr);
			br.readLine();
			String data = br.readLine();
			while(data != null)
			{
				String[] datos = data.split(",");
				int id = Integer.parseInt(datos[0]);
				String name = datos[1];
				String city =  datos[2];
				double latitude =  Double.parseDouble(datos[3]);
				double longitude =  Double.parseDouble(datos[4]);
				int dpcapacity =  Integer.parseInt(datos[5]);
				String online_date =  datos[6];
				data = br.readLine();
				stationStack.push(new VOStation(id, name, city, latitude, longitude, dpcapacity, online_date));
				stationQueue.enqueue(new VOStation(id, name, city, latitude, longitude, dpcapacity, online_date));
			}
			br.close();
			fr.close();
		}
		catch(Exception e)
		{
			System.out.println("hubo una excepci�n cargando las estaciones");
			System.out.println(e.getMessage());
		}

	}

	public void loadTrips (String tripsFile) {
		tripStack = new ListStack<VOTrip>();
		tripQueue = new ListQueue <VOTrip>();
		try
		{
			FileReader fr =  new FileReader(tripsFile);
			BufferedReader br = new BufferedReader(fr);
			br.readLine();
			String data = br.readLine();
			while(data != null)
			{
				data = data.replace('"', ' ');
				String[] datos = data.split(",");
				int trip_id = Integer.parseInt(datos[0].trim());
				String start_time = datos[1].trim();
				String end_time = datos[2].trim();
				int bikeid = Integer.parseInt(datos[3].trim());
				int tripduration = Integer.parseInt(datos[4].trim());
				int from_station_id = Integer.parseInt(datos[5].trim());
				String from_station_name = datos[6].trim();
				int to_station_id = Integer.parseInt(datos[7].trim());
				String to_station_name = datos[8].trim();
				String usertype = datos[9].trim();
				String gender;
				String birthyear;

				if(datos.length == 10)
				{
					gender = "";
					birthyear = "";
				}
				else if(datos.length == 11)
				{
					gender = datos[10].trim();
					birthyear ="";
				}
				else
				{
					gender = datos[10].trim();
					birthyear = datos[11].trim();
				}
				tripStack.push(new VOTrip(trip_id, start_time, end_time, bikeid, tripduration, from_station_id, from_station_name, to_station_id, to_station_name, usertype, gender, birthyear));
				tripQueue.enqueue(new VOTrip(trip_id, start_time, end_time, bikeid, tripduration, from_station_id, from_station_name, to_station_id, to_station_name, usertype, gender, birthyear));
				data = br.readLine();
			}
			br.close();
			fr.close();
			
		}
		catch(Exception e)
		{
			System.out.println("hubo una excepci�n cargando los viajes");
			System.out.println(e.getMessage());
		}		
	}
	@Override

	public IDoublyLinkedList <String> getLastNStations (int bicycleId, int n) {
		/*int i = 0;
		IDoublyLinkedList<String> list = new LinkedList<String>();
		Iter<VOTrip> iterator = (Iter<VOTrip>) tripStack.iterator();
		while(iterator.hasNext() && i < n)
		{
			VOTrip s = iterator.next();
			if(s.getBikeid() == bicycleId)
			{
				list.addAtBeginning(s.getFromStationName());
				i++;
			}
		}
		return list;*/
		LinkedList<String> list = new LinkedList<String>();
		Iter <VOTrip> iterator = (Iter<VOTrip>) tripQueue.iterator();
		VOTrip initial = iterator.current.getElement();
		if(initial.getBikeid() == bicycleId)
		{
			list.addAtBeginning(initial.getFromStationName());
		}
		while(iterator.hasNext())
		{
			VOTrip s = iterator.next();
			if(s.getBikeid() == bicycleId)
			{
				list.addAtBeginning(s.getFromStationName());
			}
		}
		IDoublyLinkedList<String> lastn = list.subList(0, n) ;
		return lastn;
	}

	@Override
	public VOTrip customerNumberN (int stationID, int n) {
		/*		int i = 0;
		VOTrip trip = null;
		Iter<VOTrip> iterator = (Iter<VOTrip>) tripQueue.iterator();
		while(iterator.hasNext() && i < n)
		{
			VOTrip s = iterator.next();
			if(s.getFromStationId() == stationID)
			{
				trip = s;
				i++;
			}
		}
		return trip;*/
		LinkedList<VOTrip> list = new LinkedList<VOTrip>();
		Iter <VOTrip> iterator = (Iter<VOTrip>) tripStack.iterator();
		VOTrip initial = iterator.current.getElement();
		if(initial.getToStationId() == stationID)
		{
			list.addAtBeginning(initial);
		}
		while(iterator.hasNext())
		{
			VOTrip s = iterator.next();
			if(s.getToStationId() == stationID)
			{
				list.addAtBeginning(s);
			}
		}
		VOTrip numbern = list.getElement(n-1) ;
		return numbern;
	}

	public ListStack<VOStation> getStationStack()
	{
		return stationStack;
	}
	public ListStack<VOTrip> getTripStack()
	{
		return tripStack;
	}
	public ListQueue<VOStation> getStationQueue()
	{
		return stationQueue;
	}
	public ListQueue<VOTrip> getTripQueue()
	{
		return tripQueue;
	}




}
