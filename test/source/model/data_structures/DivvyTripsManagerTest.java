package model.data_structures;
import static org.junit.Assert.*;

import javax.swing.plaf.synth.SynthSpinnerUI;

import org.junit.*;
import model.logic.DivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;

public class DivvyTripsManagerTest 
{
	private final static String stationsFile = "./test/source/data/Stations_Test.csv";
	private final static String tripsFile = "./test/source/data/Trips_Test.csv";

	private DivvyTripsManager manager;

	@Before
	public void setUpScenario1()
	{
		manager = new DivvyTripsManager();
	}

	@Test
	public void testLoadStations()
	{
		manager.loadStations(stationsFile);
		assertEquals(5, manager.getStationStack().size());
		assertEquals(5, manager.getStationQueue().size());
		Iter <VOStation> iterator = (Iter<VOStation>) manager.getStationQueue().iterator();
		int count = 1;
		assertEquals(count, iterator.current.getElement().getId());
		while(iterator.hasNext())
		{
			VOStation s = iterator.next();
			count++;
			assertEquals(count, s.getId());
		}

		Iter <VOStation> iterator1 = (Iter<VOStation>) manager.getStationStack().iterator();
		count = 5;
		assertEquals(count, iterator.current.getElement().getId());
		while(iterator1.hasNext())
		{
			VOStation t = iterator1.next();
			count--;
			assertEquals(count, t.getId());
		}
	}

	@Test
	public void testLoadTrips()
	{
		
		manager.loadTrips(tripsFile);
		assertEquals(8, manager.getTripStack().size());
		assertEquals(8, manager.getTripQueue().size());
		Iter <VOTrip> iterator = (Iter<VOTrip>) manager.getTripQueue().iterator();
		int id = 15763142;
		assertEquals(id, iterator.current.getElement().getid());
		while(iterator.hasNext())
		{
			VOTrip t = iterator.next();
			id--;
			assertEquals(id, t.getid());
		}

		
//		Iter <VOTrip> iterator1 = (Iter<VOTrip>) manager.getTripStack().iterator();
//		id = 15763135;
//
//		while(iterator.hasNext())
//		{
//			VOTrip t = iterator.next();
//			assertEquals(id++, t.getid());
//		}
		
	}

}
